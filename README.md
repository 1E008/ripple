# Installing the basics
- Run these commands to install the basics
```
sudo apt install docker.io
sudo apt install curl 
sudo apt install net-tools
sudo curl -L https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

- Then clone the repo, change to the ripple directory and build the docker-compose, which you can do with these 3 commands.
```
git clone https://gitlab.com/1E008/ripple/
cd ripple
sudo docker-compose build
```


# Configure Database
Change the following:
- Password in docker-compose.yml under "MYSQL_ROOT_PASSWORD="
- Database password in database.sh, replacing "pass" after "-p" with your mysql password
So instead of (WITHOUT THE BRACKETS)
```
mysql -u root -ppass << EOF
```
it would be
```
mysql -u root -p(password) << EOF
```

- Then run these 2 commands 
```
sudo docker-compose up -d db
sudo ./database.sh
```

### Get pep.py config file

    sudo docker run -v $PWD:/out pep.py /bin/bash -c "python3.6 pep.py && cp config.ini /out/peppy.ini"

In "peppy.ini", change the following:
- Note: If it's "locked", run 
```
sudo chown (user) peppy.ini
```
- Change your database password under the [db] section
- Change your ci-key (This can be anything, remember it you'll need it later)

#### Get Let's config file

    sudo docker run -v $PWD:/out let.s /bin/bash -c "python3.6 lets.py && cp config.ini /out/lets.ini"

In "lets.ini", change the following:
- Note: If it's "locked", run 
```
sudo chown (user) lets.ini
```
- Your database password under the [db] section
- Your cikey(server/apikey) (From before!!)
- Add your osu! API key, via https://osu.ppy.sh/p/api (Have to be on old site version)

#### Get hanayo config file

    sudo docker run -v $PWD:/out hanayo sh -c "./hanayo && cp hanayo.conf /out/hanayo.conf"

In "hanayo.conf" change the following lines:
- Note: If it's "locked", run 
```
sudo chown (user) hanayo.conf
```
- ListenTo to ":6969" (Yes, including the ":")
- DSN=root:(your root password)@tcp(db:3306)/ripple
- RedisEnable=true
- AvatarURL to https://a.domain
- BaseURL to https://domain
- API to http://rippleapi:40001/api/v1/
- BanchoAPI to https://c.domain
- Set APISecret
- AvatarFolder to /avatars
- RedisAddress to redis:6379
- (This doesn't matter too much) MainRippleFolder to ??? (MainRippleFolder/ci-system/ci-system/changelog.txt)

### Get rippleapi config file 

    sudo docker run -v $PWD:/out rippleapi sh -c "./rippleapi && cp api.conf /out/rippleapi.conf"

In "rippleapi.conf" change the following lines:
- Note: If it's "locked", run 
```
sudo chown (user) rippleapi.conf
```
- DSN to root:(your root password)@tcp(db:3306)/ripple
- hanayo api secret
- Your osu! API key
- RedisAddr=redis:6379

## Old Frontend

```
git clone -b Dockerfile --recurse-submodules https://gitlab.com/1E008/old-frontend/
sudo docker run --rm -v $PWD/old-frontend:/app/ composer install
cp old-frontend/inc/config.sample.php oldfrontend.config.php
```

Update oldfrontend.config.php
- Edit the lines to your information under "// Database config"
- Edit the lines to your information under "// Server urls, no slash" (You don't need to edit scores line, just the avatar, server and banch)

## nginx

```
mkdir keys
sudo openssl req -x509 -nodes -days 99999 -newkey rsa:2048 -keyout ./keys/ppy.sh.key.pem -out ./keys/ppy.sh.cert.pem -subj "/C=US/ST=Denial/L=Springfield/O=Dis/CN=*.ppy.sh"
```

- Edit [nginx.conf](nginx.conf)
- Replace test.test with your domain
- You need to download/import the ./keys/ppy.sh.cert.pem on your main PC/OS

## Avatar Server

    mkdir avatars

- Add default 0.png into ./avatars/

# You're done! (hopefully) time to launch the server!

You can now launch the server with:

    sudo docker-compose up -d web
    
Every time you turn off your Virtual Machine or restart the server, you'll need to run this command

# So.. what now? Database/Website management

You can access portainer to manage your docker containers under domain:9000. Portainer can be used to check logs and container statuses etc.

    sudo docker-compose up -d portainer

You can access phpmyadmin under domain:8056.

    sudo docker-compose up -d phpmyadmin

Don't leave these open in production. lol

## Todo:
- Need to mention to gain full ownership of the file do "sudo chown (user) (file)"
- Need to mention AdminCP is on the old.website
- Need to mention to allow the avatar server through the browser (So go on your userpage, right click your avatar, copy image address, then trust it)
- Maybe show how to make a server Switcher?
- (If VirtualBox) Network adapter 2 -> host-only -> virtualbox host-only then Global tools (to the far right), check IP, then go in Ubuntu and run ifconfig, IP will be the one similar to the one in Global tools (192.168.x), most likely inet
- (If VirtualBox) Editing the hosts file (need to change the following https://pastebin.com/4HGg0GZ7)
- Fork the ripple-docker (Whatever you want to fork, so if you needed the website, hanayo) and then edit the URL in docker-compose.yml to redirect it to your own custom one. (Make sure to edit in the #DockerFile branch) 
- sudo docker-compose pull
- sudo docker-compose build
- Restart the portainer